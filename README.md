﻿# EDXFramework

A little C++ Game Framework for Windows

# Features
* Game class XNA way
* OpenGL
* 3D Model Loader
* Gamepad handler

# Dependencies
* [DirectX SDK](http://www.microsoft.com/en-us/download/details.aspx?id=6812)

# EDXFramework doesn't have
* Physics
* Does not have scripting

