#pragma once
#include "Game.h"
#include "Model.h"
#include "Camera.h"
#include "GamepadGeneric.h"

#include <string>
using namespace std;

using namespace EDXFramework;

class GameWithGamepadSample : public Game {
public:
	GameWithGamepadSample();
	virtual ~GameWithGamepadSample();
	void Initialize();
	void LoadContent();
	void Update();
	void Draw();
	void UnloadContent();

protected:
	float tx, ty, tz;
	float angle;
	void drawCube(float size = 1);
	void debugPositions();
	void updateCamera();
	Model* model;
	Model* gunM9;
	BMPClass cubeTexture;

	Camera *mainCamera;
	GamepadGeneric *gamepad;

	unsigned int cubeTextureId;
};