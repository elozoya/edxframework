#pragma once
#include "GameBase.h"
#include "GameObject.h"

using namespace EDXFramework;

class GameTransformations : public GameBase {
public:
	GameTransformations();
	~GameTransformations();
	void Initialize();
	void LoadContent();
	void Update();
	void Draw();
	void UnloadContent();
private:
	GameObject* gameObject;
};
