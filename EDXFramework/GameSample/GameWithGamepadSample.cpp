#include "GameWithGamepadSample.h"
#include "Keyboard.h"
#include "Random.h"
#include <gl\GL.h>
#include <gl\GLU.h>
#include "Console.h"
#include "Helper.h"


GameWithGamepadSample::GameWithGamepadSample() : Game() {
	tx = ty = 1;
	tz = 0;
	angle = 0;
	//Default Window Size
	//this->SetWindowSize(800, 600);
}

GameWithGamepadSample::~GameWithGamepadSample() {
	delete mainCamera;
	delete gamepad;

	delete model;
	delete gunM9;
}

void GameWithGamepadSample::Initialize() {
	Game::Initialize();
	// Enable depth test
	glEnable(GL_DEPTH_TEST);
	// Accept fragment if it closer to the camera than the former one
	glDepthFunc(GL_LESS);
	// Enable 2D texturing
	glEnable(GL_TEXTURE_2D);
	// Choose a smooth shading model
    glShadeModel(GL_SMOOTH);
	// Set the clear color to white
	glClearColor(1.0, 1.0, 1.0, 1.0);

	// Enable the alpha test. This is needed
	// to be able to have images with transparent
	// parts.
	glEnable(GL_ALPHA_TEST);
	glAlphaFunc(GL_GREATER, 0.0f);

	//Enable Light 0
	GLfloat light_ambient[] = { 1.0, 1.0, 1.0, 1.0 };
	GLfloat light_diffuse[] = { 1.0, 1.0, 1.0, 1.0 };
	GLfloat light_specular[] = { 1.0, 1.0, 1.0, 1.0 };
	GLfloat light_position[] = { 1.0, 1.0, 1.0, 0.0 };
	glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
	glLightfv(GL_LIGHT0, GL_POSITION, light_position);
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);

	//Ambient Light
	GLfloat lmodel_ambient[] = { 1, 1, 1, 1.0 };
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);

	// Enable blending
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}

void GameWithGamepadSample::LoadContent() {
	model = new Model("models//Cyborg_Riffle2//Cyborg_Riffle.obj", "models//Cyborg_Riffle2//Cyborg_Riffle_D.bmp");
	//Dibujar modelo sin textura
	gunM9 = new Model("models//M9//M9.obj");

	//Cargar una textura para el cubo
	BMPError e = BMPLoad("textures//cielo1.bmp", cubeTexture);
	if (e == BMPNOERROR) {
		glGenTextures(1, &cubeTextureId);
		glBindTexture(GL_TEXTURE_2D, cubeTextureId);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_CLAMP);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_CLAMP);
		glTexImage2D(GL_TEXTURE_2D, 0, 3, cubeTexture.width, cubeTexture.height, 0, GL_RGB,GL_UNSIGNED_BYTE, cubeTexture.bytes);
	}

	// Inicializacion de la camara con posicion (0|0|0) y rotacion (0|0|0)
	mainCamera = new Camera();

	gamepad = new GamepadGeneric();
}

void GameWithGamepadSample::Update() {
	
	// Metodo para actualizar el estado de la camara con ayuda del gamepad
	this->updateCamera();

	if (Keyboard::IsKeyDown(VK_ESCAPE)) {
		this->Exit();
	}
	float step = 1;
	float elapsedTimeSeconds = this->elapsedTime / 1000.0;

	if (Keyboard::IsKeyDown(VK_RIGHT)) {
		tx += step * elapsedTimeSeconds;
		debugPositions();
	}
	if (Keyboard::IsKeyDown(VK_LEFT)) {
		tx += -step * elapsedTimeSeconds;
		debugPositions();
	}
	if (Keyboard::IsKeyDown(VK_UP)) {
		ty += step * elapsedTimeSeconds;
		debugPositions();
	}
	if (Keyboard::IsKeyDown(VK_DOWN)) {
		ty += -step * elapsedTimeSeconds;
		debugPositions();
	}
	if (Keyboard::IsKeyDown('W')) {
		tz += step * elapsedTimeSeconds;
		debugPositions();
	}
	if (Keyboard::IsKeyDown('S')) {
		tz += -step * elapsedTimeSeconds;
		debugPositions();
	}
	GLfloat lightPos[4] = {tx, ty , tz, 0.0};
	glLightfv(GL_LIGHT0, GL_POSITION, lightPos);
	angle += 180 * elapsedTimeSeconds;
}

void GameWithGamepadSample::Draw() {
	//glLoadIdentity();
	

	gluLookAt(3, 3, 3, 0, 0, 0, 0, 1, 0);

	// Here goes the drawing code
	glPushMatrix();
	glTranslatef(tx, ty, tz);
	model->Draw();
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0, 0, 0);
	glRotatef(angle, 0, 1, 0);
	this->drawCube(0.5);
	glPopMatrix();

	glPushMatrix();
	gunM9->Draw();
	glPopMatrix();
	
	Game::Draw();
}

void GameWithGamepadSample::UnloadContent() {
}

void GameWithGamepadSample::updateCamera(){
	// Cargar matriz de indentidad para reiniciar la matriz principal en este caso MODELVIEW
	glLoadIdentity();

	if(gamepad->IsConnected()){
		// Obtener el estado del gamepad
		GAMEPAD_STATE gamepadState = gamepad->GetState();
		float elapsedTimeSeconds = this->elapsedTime / 1000.0;

		float moveFactor = 1.5f;
		float rotateFactor = 3.0f;

		float moveX = ((gamepadState.THUMB_LX / 64.0f) * elapsedTimeSeconds) * moveFactor;
		float moveY = ((gamepadState.THUMB_LY / 64.0f) * elapsedTimeSeconds)  * moveFactor;
		mainCamera->MoveForward(moveY);
		mainCamera->StrafeRight(moveX);

		float rotateY = ((gamepadState.THUMB_RX / 64.0f)  * elapsedTimeSeconds) * rotateFactor;
		float rotateX = ((gamepadState.THUMB_RY / 64.0f)  * elapsedTimeSeconds) * rotateFactor;
		mainCamera->RotateX(-rotateX);
		mainCamera->RotateY(-rotateY);

		
	}
	// Una vez actualizados los valores de la camara, se procede a actualizar la nueva matriz.
	mainCamera->Update();
	
}
void GameWithGamepadSample::debugPositions() {
	string msg = "tx: " + Helper::toString(tx) + " ty: " + Helper::toString(ty) + " tz: " + Helper::toString(tz);
	Console::Log(msg);
}

void GameWithGamepadSample::drawCube(float size) {
	GLfloat red[]    = { 1, 0, 0, 0.8 };
	GLfloat green[]  = { 0, 1, 0, 1.0 };
	GLfloat blue[]   = { 0, 0, 1, 1.0 };
	GLfloat white[]  = { 1, 1, 1, 1.0 };
	GLfloat yellow[] = { 1, 1, 0, 1.0 };

	glBindTexture(GL_TEXTURE_2D, cubeTextureId);
	// FRONT
	glBegin(GL_QUADS);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, yellow);
	glNormal3f(0, 0, 1);
	glVertex3f(  size, -size, size );
	glVertex3f(  size,  size, size );
	glVertex3f( -size,  size, size );
	glVertex3f( -size, -size, size );
	glEnd();

	// BACK
	glBegin(GL_QUADS);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, red);
	glNormal3f(0, 0, -1);
	glVertex3f(  size, -size, -size );
	glVertex3f(  size,  size, -size );
	glVertex3f( -size,  size, -size );
	glVertex3f( -size, -size, -size );
	glEnd();

	// RIGHT
	glBegin(GL_QUADS);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, green);
	glNormal3f(1, 0, 0);
	glVertex3f( size, -size, -size );
	glVertex3f( size,  size, -size );
	glVertex3f( size,  size,  size );
	glVertex3f( size, -size,  size );
	glEnd();

	// LEFT
	glBegin(GL_QUADS);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, blue);
	glNormal3f(-1, 0, 0);
	glVertex3f( -size, -size,  size );
	glVertex3f( -size,  size,  size );
	glVertex3f( -size,  size, -size );
	glVertex3f( -size, -size, -size );
	glEnd();

	// TOP
	glBegin(GL_QUADS);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, white);
	glNormal3f(0, 1, 0);
	glTexCoord2f(1, 0);
	glVertex3f(  size,  size,  size );
	glTexCoord2f(0, 0);
	glVertex3f( -size,  size,  size );
	glTexCoord2f(0, 1);
	glVertex3f( -size,  size, -size );
	glTexCoord2f(1, 1);
	glVertex3f(  size,  size, -size );
	glEnd();

	// BOTTOM
	glBegin(GL_QUADS);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, yellow);
	glNormal3f(0, -1, 0);
	glVertex3f(  size, -size, -size );
	glVertex3f(  size, -size,  size );
	glVertex3f( -size, -size,  size );
	glVertex3f( -size, -size, -size );
	glEnd();
	glBindTexture(GL_TEXTURE_2D, NULL);
}

