#pragma once
#include "GameBase.h"
#include "Model.h"

using namespace EDXFramework;

class GameLoadModel : public GameBase {
public:
	GameLoadModel();
	~GameLoadModel();
	void Initialize();
	void LoadContent();
	void Update();
	void Draw();
	void UnloadContent();
private:
	Model* model;
};
