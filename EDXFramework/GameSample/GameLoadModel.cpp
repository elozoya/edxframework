#include "GameLoadModel.h"
#include "Keyboard.h"

GameLoadModel::GameLoadModel() : GameBase() {
	this->SetWindowTitle("GameLoadModel");
}

GameLoadModel::~GameLoadModel() {
	delete model;
}

void GameLoadModel::Initialize() {
	GameBase::Initialize();
	// Set the clear color to blue
	glClearColor(0.0, 0.0, 1.0, 1.0);
}

void GameLoadModel::LoadContent() {
	model = new Model("models//Cyborg_Riffle2//Cyborg_Riffle.obj", "models//Cyborg_Riffle2//Cyborg_Riffle_D.bmp");
	//Dibujar modelo sin textura
	//model = new Model("models//Cyborg_Riffle2//Cyborg_Riffle.obj");
}

void GameLoadModel::Update() {
	if (Keyboard::IsKeyDown(VK_ESCAPE)) {
		this->Exit();
	}
}

void GameLoadModel::Draw() {
	glLoadIdentity();
	gluLookAt(3, 3, 3, 0, 0, 0, 0, 1, 0);
	model->Draw();
}

void GameLoadModel::UnloadContent() {
}

