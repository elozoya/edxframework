#include "GameShader.h"

GameShader::GameShader() : GameWithGamepadSample() {
	fade = 0.0;
}

GameShader::~GameShader() {
}

void GameShader::Initialize() {
	GameWithGamepadSample::Initialize();
	glClearColor(1.0, 1.0, 1.0, 1.0);
}

void GameShader::LoadContent() {
	GameWithGamepadSample::LoadContent();
	shader = Shader("shaders//simple.v.glsl", "shaders//simple.f.glsl");
	shader.SetUniform("fade", fade);
}

void GameShader::Update() {
	GameWithGamepadSample::Update();
	float elapsedTimeSeconds = this->elapsedTime / 1000.0;
	fade += 0.5 * elapsedTimeSeconds;
	fade = (fade > 1.0) ? 0.0 : fade;
	shader.SetUniform("fade", fade);
}

void GameShader::Draw() {
	GameWithGamepadSample::Draw();

	shader.Use();
	glBegin(GL_TRIANGLES);
	glVertex2f(0.0, 0.8);
	glVertex2f(-0.8, -0.8);
	glVertex2f(0.8, -0.8);
	glEnd();
	shader.Disable();

	Game::Draw();
}

void GameShader::UnloadContent() {
}
