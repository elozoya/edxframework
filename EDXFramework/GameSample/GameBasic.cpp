#include "GameBasic.h"
#include "Keyboard.h"

GameBasic::GameBasic() : GameBase() {
	//Default Window Size
	this->SetWindowSize(800, 600);
	this->SetWindowTitle("GameBasic");
}

GameBasic::~GameBasic() {
}

void GameBasic::Initialize() {
	GameBase::Initialize();
	// Set the clear color to blue
	glClearColor(0.0, 0.0, 1.0, 1.0);
}

void GameBasic::LoadContent() {
}

void GameBasic::Update() {
	if (Keyboard::IsKeyDown(VK_ESCAPE)) {
		this->Exit();
	}
}

void GameBasic::Draw() {
}

void GameBasic::UnloadContent() {
}

