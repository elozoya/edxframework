#include <windows.h>
#include "GameBasic.h"
#include "GameLoadModel.h"
#include "GameTransformations.h"
#include "GameWithGamepadSample.h"
#include "GameShader.h"
using namespace EDXFramework;

int WINAPI WinMain(HINSTANCE hInst, HINSTANCE hPrevInst, LPSTR lpCmdLine, int nShowCmd) {
	Game* game = new GameBasic();
	//Game* game = new GameLoadModel();
	//Game* game = new GameTransformations();
	//Game* game = new GameWithGamepadSample();
	//Game* game = new GameShader();
	game->Run();
	delete game;
	return 0;
}