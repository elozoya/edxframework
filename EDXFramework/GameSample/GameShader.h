#pragma once
#include "GameWithGamepadSample.h"
#include "Shader.h"

using namespace EDXFramework;
using namespace EDXFramework::Shaders;

class GameShader : public GameWithGamepadSample {
public:
	GameShader();
	~GameShader();
	void Initialize();
	void LoadContent();
	void Update();
	void Draw();
	void UnloadContent();

private:
	Shader shader;
	float fade;
};