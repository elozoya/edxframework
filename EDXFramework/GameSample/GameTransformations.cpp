#include "GameTransformations.h"
#include "Keyboard.h"

GameTransformations::GameTransformations() : GameBase() {
	this->SetWindowTitle("GameTransformations");
}

GameTransformations::~GameTransformations() {
	delete gameObject;
}

void GameTransformations::Initialize() {
	GameBase::Initialize();
	// Set the clear color to blue
	glClearColor(0.0, 0.0, 1.0, 1.0);
}

void GameTransformations::LoadContent() {
	gameObject = new GameObject("models//Cyborg_Riffle2//Cyborg_Riffle.obj", "models//Cyborg_Riffle2//Cyborg_Riffle_D.bmp");
}

void GameTransformations::Update() {
	if (Keyboard::IsKeyDown(VK_ESCAPE)) {
		this->Exit();
	}
	float step = 1, step2 = 50;
	float elapsedTimeSeconds = this->elapsedTime / 1000.0;

	if (Keyboard::IsKeyDown(VK_RIGHT)) {
		gameObject->transform.Translate(step * elapsedTimeSeconds, 0, 0);
	}
	if (Keyboard::IsKeyDown(VK_LEFT)) {
		gameObject->transform.Translate(-step * elapsedTimeSeconds, 0, 0);
	}
	if (Keyboard::IsKeyDown(VK_UP)) {
		gameObject->transform.Translate(0, step * elapsedTimeSeconds, 0);
	}
	if (Keyboard::IsKeyDown(VK_DOWN)) {
		gameObject->transform.Translate(0, -step * elapsedTimeSeconds, 0);
	}
	if (Keyboard::IsKeyDown('W')) {
		gameObject->transform.Translate(0, 0, step * elapsedTimeSeconds);
	}
	if (Keyboard::IsKeyDown('S')) {
		gameObject->transform.Translate(0, 0, -step * elapsedTimeSeconds);
	}
	if (Keyboard::IsKeyDown('X') && !Keyboard::IsKeyDown(VK_LSHIFT)) {
		gameObject->transform.Rotate(step2 * elapsedTimeSeconds, 0, 0);
	}
	if (Keyboard::IsKeyDown('X') && Keyboard::IsKeyDown(VK_LSHIFT)) {
		gameObject->transform.Rotate(-step2 * elapsedTimeSeconds, 0, 0);
	}
	if (Keyboard::IsKeyDown('Y') && !Keyboard::IsKeyDown(VK_LSHIFT)) {
		gameObject->transform.Rotate(0, step2 * elapsedTimeSeconds, 0);
	}
	if (Keyboard::IsKeyDown('Y') && Keyboard::IsKeyDown(VK_LSHIFT)) {
		gameObject->transform.Rotate(0, -step2 * elapsedTimeSeconds, 0);
	}
	if (Keyboard::IsKeyDown('Z') && !Keyboard::IsKeyDown(VK_LSHIFT)) {
		gameObject->transform.Rotate(0, 0, step2 * elapsedTimeSeconds);
	}
	if (Keyboard::IsKeyDown('Z') && Keyboard::IsKeyDown(VK_LSHIFT)) {
		gameObject->transform.Rotate(0, 0, -step2 * elapsedTimeSeconds);
	}

}

void GameTransformations::Draw() {
	glLoadIdentity();
	gluLookAt(3, 3, 3, 0, 0, 0, 0, 1, 0);
	gameObject->Draw();
}

void GameTransformations::UnloadContent() {
}

