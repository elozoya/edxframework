#version 120

uniform float fade;

void main() {
	gl_FragColor = vec4(1.0, 0.0, 0.0, fade);
}
