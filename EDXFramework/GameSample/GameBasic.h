#pragma once
#include "GameBase.h"
using namespace EDXFramework;

class GameBasic : public GameBase {
public:
	GameBasic();
	~GameBasic();
	void Initialize();
	void LoadContent();
	void Update();
	void Draw();
	void UnloadContent();
};
