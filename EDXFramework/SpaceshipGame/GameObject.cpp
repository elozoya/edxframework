#include "GameObject.h"

GameObject::GameObject() {
}

GameObject::GameObject(Model* model) {
	this->model = model;
}

GameObject::~GameObject() {
}

void GameObject::Draw() {
	glPushMatrix();
	glTranslatef(transform.tx, transform.ty, transform.tz);
	glRotatef(transform.rx, 1.0, 0.0, 0.0);
	glRotatef(transform.ry, 0.0, 1.0, 0.0);
	glRotatef(transform.rz, 0.0, 0.0, 1.0);
	model->Draw();
	glPopMatrix();
}

Vector3D GameObject::GetPosition() {
	return Vector3D(transform.tx, transform.ty, transform.tz);
}

void GameObject::Translate(float tx, float ty, float tz) {
	transform.tx += tx;
	transform.ty += ty;
	transform.tz += tz;
}

void GameObject::Rotate(float x, float y, float z) {
	transform.rx += x;
	transform.ry += y;
	transform.rz += z;
}

void GameObject::SetPosition(float x, float y, float z) {
	transform.tx = x;
	transform.ty = y;
	transform.tz = z;
}

void GameObject::SetRotation(float x, float y, float z) {
	transform.rx = x;
	transform.ry = y;
	transform.rz = z;
}

Transform GameObject::GetTransform() {
	return transform;
}