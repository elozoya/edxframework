#include "SpaceshipGame.h"
#include "Random.h"
#include <gl\GL.h>
#include <gl\GLU.h>
#include "Console.h"
#include "Helper.h"
#include "Vector3D.h"


SpaceshipGame::SpaceshipGame() : Game() {
	tx = ty = tz = 0;
	enemyTimeElapsed = 0;
	spaceshipVelocity = 60;
	enemyVelocity = 40;
	fireVelocity = 80;
	cameraMode = "mode1";
	//Default Window Size
	//this->SetWindowSize(800, 600);
}

SpaceshipGame::~SpaceshipGame() {
	delete spaceship;
	delete enemy;
	delete fire;
}

void SpaceshipGame::Initialize() {
	// Enable depth test
	glEnable(GL_DEPTH_TEST);
	// Accept fragment if it closer to the camera than the former one
	glDepthFunc(GL_LESS);
	// Enable 2D texturing
	glEnable(GL_TEXTURE_2D);
	// Choose a smooth shading model
    glShadeModel(GL_SMOOTH);
	// Set the clear color to white
	glClearColor(0.0, 0.0, 0.0, 1.0);

	// Enable the alpha test. This is needed
	// to be able to have images with transparent
	// parts.
	glEnable(GL_ALPHA_TEST);
	glAlphaFunc(GL_GREATER, 0.0f);

	//Enable Light 0
	GLfloat light_ambient[] = { 1.0, 1.0, 1.0, 1.0 };
	GLfloat light_diffuse[] = { 1.0, 1.0, 1.0, 1.0 };
	GLfloat light_specular[] = { 1.0, 1.0, 1.0, 1.0 };
	GLfloat light_position[] = { 1.0, 1.0, 1.0, 0.0 };
	glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
	glLightfv(GL_LIGHT0, GL_POSITION, light_position);
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);

	//Ambient Light
	GLfloat lmodel_ambient[] = { 1, 1, 1, 1.0 };
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);

	// Enable blending
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

}

void SpaceshipGame::LoadContent() {
	spaceship = new Model("models//player.obj");
	fire = new Model("models//fire.obj");
	enemy = new Model("models//enemy.obj");
	spaceshipGameObject = GameObject(spaceship);
	enemyRadio = enemy->GetRadio();
	fireRadio = fire->GetRadio();
	for(int i = -2; i < 3; i++) {
		enemyStartPositions.push_back(Vector3D(i * enemyRadio, 0, -80));
	}
	spaceshipShader = Shader("shaders//spaceship.v.glsl", "shaders//spaceship.f.glsl");
	spaceshipShader.SetUniform("color", 0.0, 0.0, 1.0, 0.5);
}

void SpaceshipGame::Update() {
	float elapsedTimeSeconds = this->elapsedTime / 1000.0;
	if (GetKeyState(VK_ESCAPE) & 8000) {
		this->Exit();
	}
	enemyTimeElapsed += this->elapsedTime;
	if (enemyTimeElapsed >= 500) {
		enemyTimeElapsed = 0;
		addEnemy();
	}
	for(vector<GameObject>::iterator it = enemies.begin(); it != enemies.end(); ) {
		it->Translate(0, 0, enemyVelocity * elapsedTimeSeconds);
		if (it->GetTransform().tz <= SpaceshipGame::ENEMY_LIMIT_END) {
			it = enemies.erase(it);
			continue;
		}
		it++;
	}
	for(vector<GameObject>::iterator it = fires.begin(); it != fires.end();) {
		it->Translate(0, 0,-fireVelocity * elapsedTimeSeconds); 
		if (it->GetTransform().tz <= -100) {
			it = fires.erase(it);
			continue;
		}
		bool tmp = false;
		for(vector<GameObject>::iterator it2 = enemies.begin(); it2 != enemies.end();) {
			if (sphereCollision(it->GetPosition(), it2->GetPosition(), enemyRadio, fireRadio)) {
				it2 = enemies.erase(it2);
				it = fires.erase(it);
				tmp = true;
				break;
			}
			it2++;
		}
		if (!tmp) {
			it++;
		}
	}
	if (spaceshipGameObject.GetTransform().tx <= MAX && GetKeyState(VK_RIGHT) & 8000) {
		spaceshipGameObject.Translate(spaceshipVelocity * elapsedTimeSeconds, 0, 0);
		spaceshipGameObject.SetRotation(0, 0, -45);

	} else if (spaceshipGameObject.GetTransform().tx >= -MAX && GetKeyState(VK_LEFT) & 8000) {
		spaceshipGameObject.Translate(-spaceshipVelocity * elapsedTimeSeconds, 0, 0);
		spaceshipGameObject.SetRotation(0, 0, 45);
	} else {
		spaceshipGameObject.SetRotation(0, 0, 0);
	}
	if (GetKeyState(VK_UP) & 8000) {
		spaceshipGameObject.Translate(0, 0, -spaceshipVelocity * elapsedTimeSeconds);
	}
	if (GetKeyState(VK_DOWN) & 8000) {
		spaceshipGameObject.Translate(0, 0, spaceshipVelocity * elapsedTimeSeconds);
	}
	if (GetKeyState('F') & 8000) {
		addFire();
	}
	if (GetKeyState('1') & 8000) {
		cameraMode = "mode1";
	}
	if (GetKeyState('2') & 8000) {
		cameraMode = "mode2";
	}
}

void SpaceshipGame::Draw() {
	// Clear the buffers
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();
	if (cameraMode == "mode1") {
		float eye = 200;
		gluLookAt(0, eye, 0, 0, 0, 0, 0, 0, -1);
	} else {
		//Set camera without look at
		glRotatef(30, 1.0, 0, 0);
		glTranslatef(0, -100, -150);
	}
	spaceshipShader.Use();
	spaceshipShader.SetUniform("color", 0.0, 0.0, 1.0, 1.0);
	spaceshipShader.SetUniform("light", 0.0, 1.0, 0.0);
	spaceshipGameObject.Draw();
	spaceshipShader.SetUniform("color", 1.0, 0.0, 0.0, 1.0);
	spaceshipShader.SetUniform("light", 0.0, -1.0, 0.0);
	for(vector<GameObject>::iterator it = enemies.begin(); it != enemies.end(); it++) {
		it->Draw();
	}
	spaceshipShader.SetUniform("color", 0.0, 1.0, 0.0, 1.0);
	spaceshipShader.SetUniform("light", 0.0, 1.0, 0.0);
	for(vector<GameObject>::iterator it = fires.begin(); it != fires.end(); it++) {
		it->Draw();
	}
	spaceshipShader.Disable();
	Game::Draw();
}

void SpaceshipGame::UnloadContent() {
}

void SpaceshipGame::addEnemy() {
	int index = random.getRandom(0, enemyStartPositions.size() - 1);
	GameObject enemyObject = GameObject(enemy);
	enemyObject.SetPosition(enemyStartPositions[index].x, enemyStartPositions[index].y, enemyStartPositions[index].z);
	enemyObject.SetRotation(180, 0, 0);
	enemies.push_back(enemyObject);
}

void SpaceshipGame::addFire() {
	GameObject fireObject = GameObject(fire);
	Vector3D position = spaceshipGameObject.GetPosition();
	fireObject.SetPosition(position.x, position.y, position.z + -(spaceship->GetRadio()/2));
	fires.push_back(fireObject);
}

bool SpaceshipGame::sphereCollision(Vector3D& p1,Vector3D& p2, float radio1, float radio2) {
	Vector3D v = Vector3D(p1.x - p2.x, p1.y - p2.y, p1.z - p2.z);
	float minDistance = radio1 + radio2;
	float distance = v.x * v.x + v.y * v.y + v.z * v.z;
	bool result = distance <= minDistance;
	return result;
}