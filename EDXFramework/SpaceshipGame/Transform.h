#pragma once

namespace EDXFramework {

class Transform {
public:
	Transform();
	virtual ~Transform();
	float tx, ty, tz;
	float rx, ry, rz;
	float sx, sy, sz;
};

}