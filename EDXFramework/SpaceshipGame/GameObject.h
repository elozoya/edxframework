#pragma once
#include <string>
using namespace std;
#include "glm.h"
#include "BMPLoader.h"
#include "Transform.h"
#include "Vector3D.h"
#include "Model.h"

using namespace EDXFramework;

class GameObject {
public:
	GameObject();
	GameObject(Model* model);
	virtual ~GameObject();
	void Draw();
	Vector3D GetSize();
	float GetRadio();
	Vector3D GetPosition();
	void Translate(float tx, float ty, float tz);
	void Rotate(float x, float y, float z);
	void SetPosition(float x, float y, float z);
	void SetRotation(float x, float y, float z);
	Transform GetTransform();
private:
	Transform transform;
	Model* model;
	Vector3D position;
	Vector3D velocity;
};