#pragma once
#include "Game.h"
#include "Model.h"
#include "Random.h"
#include <string>
#include <vector>
#include "Shader.h"
#include "GameObject.h"
using namespace std;

using namespace EDXFramework;
using namespace EDXFramework::Shaders;

class SpaceshipGame : public Game {
public:
	SpaceshipGame();
	~SpaceshipGame();
	void Initialize();
	void LoadContent();
	void Update();
	void Draw();
	void UnloadContent();

private:
	static const int MAX = 70;
	static const int ENEMY_LIMIT_END = -170;
	int enemyTimeElapsed;
	Random random;
	float tx, ty, tz;
	Model* spaceship;
	Model* enemy;
	Model* fire;
	GameObject spaceshipGameObject;

	vector<GameObject> enemies;
	vector<GameObject> fires;
	string cameraMode;

	vector<Vector3D> enemyStartPositions;
	float enemyVelocity;
	float spaceshipVelocity;
	float fireVelocity;
	float enemyRadio;
	float fireRadio;

	void addEnemy();
	void addFire();
	bool sphereCollision(Vector3D& p1,Vector3D& p2, float radio1, float radio2);
	Shader spaceshipShader;
};