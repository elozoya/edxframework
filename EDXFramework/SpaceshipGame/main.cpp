#include <windows.h>
#include "SpaceshipGame.h"
using namespace EDXFramework;

int WINAPI WinMain(HINSTANCE hInst,HINSTANCE hPrevInst,LPSTR lpCmdLine,int nShowCmd) {
	Game* game = new SpaceshipGame();
	game->Run();
	delete game;
	return 0;
}