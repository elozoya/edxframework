#version 120

uniform float fade;
varying vec3 vNormal;

void main(void) {
	vNormal = gl_Normal;
	//float d = fade;
    gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
}
