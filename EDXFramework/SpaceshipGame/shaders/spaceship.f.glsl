#version 120

uniform float fade = 1.0;
uniform vec4 color;
varying vec3 vNormal;
uniform vec3 light;

void main() {
	//vec4 d = color;
	//gl_FragColor = vec4(1.0, 0.0, 0.0, fade);
	//gl_FragColor = color;
	// calc the dot product and clamp
    // 0 -> 1 rather than -1 -> 1
    //vec3 light = vec3(0.5,0.2,1.0);
	//vec3 light = vec3(0.0,1.0,0.0);
    // ensure it's normalized
    vec3 lightT = normalize(light);
  
    // calculate the dot product of
    // the light to the vertex normal
    float dProd = max(0.0, dot(vNormal, lightT));
  
    // feed into our frag colour
	gl_FragColor = color;
	gl_FragColor = vec4(dProd * color.x, dProd * color.y, dProd * color.z, color.w);
}
