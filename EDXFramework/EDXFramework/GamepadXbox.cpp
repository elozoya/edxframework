#include "GamepadXbox.h"

GamepadXbox::GamepadXbox(int player_id) : Gamepad()
{
	input_id = player_id - 1;
	gamepad_type = GAMEPAD_TYPE::XBOX_CONTROLLER;
}

GAMEPAD_STATE GamepadXbox::GetState()
{
	ZeroMemory(&input_state, sizeof(XINPUT_STATE));
	XInputGetState(input_id, &input_state);
	
	gamepad_state.A = input_state.Gamepad.wButtons & XINPUT_GAMEPAD_A;
	gamepad_state.B = input_state.Gamepad.wButtons & XINPUT_GAMEPAD_B;
	gamepad_state.Y = input_state.Gamepad.wButtons & XINPUT_GAMEPAD_Y;
	gamepad_state.X = input_state.Gamepad.wButtons & XINPUT_GAMEPAD_X;
	
	return gamepad_state;
}

bool GamepadXbox::IsConnected()
{
	DWORD result;
	result = XInputGetState(input_id, &input_state);

	if(result == ERROR_SUCCESS)
		return true;
	else
		return false;
}

void GamepadXbox::Vibrate(int leftValue, int rightValue)
{
	XINPUT_VIBRATION input_vibration;

	ZeroMemory(&input_vibration, sizeof(XINPUT_VIBRATION));

	input_vibration.wLeftMotorSpeed = leftValue;
	input_vibration.wRightMotorSpeed = rightValue;

	XInputSetState(input_id, &input_vibration);
}

void GamepadXbox::Update(){
}

GamepadXbox::~GamepadXbox(){
}