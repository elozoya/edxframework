#include "Game.h"
#include <iostream>
#include "GameWindow.h"
#include "Helper.h"
#include <exception>

using EDXFramework::Game;
using EDXFramework::GUI::GameWindow;

Game::Game() {
	frameTime = 32;
	startTime = totalElapsedTime = elapsedTime = elapsedTimeSeconds = 0;
	this->SetWindowSize(800, 600);
	this->SetWindowPosition(0, 0);
}

Game::~Game() {
	delete gameWindow;
}

void Game::PreInitialize() {
	//Se crea este metodo porque
	//No quiero obligar a que las clases hijas en el metodo Initialize manden a llamar el metodo Game::Initialize

	//Si en un futuro se utiliza GLUT o FREEGLUT hay que descomentar las siguientes lineas
	//glutInitDisplayMode(GLUT_RGB);
	//glutInit(&__argc, __argv);
	unsigned int errorCode = glewInit();
	if (GLEW_OK != errorCode) {
		throw std::runtime_error("GLEW failed! : " + Helper::toString(glewGetErrorString(errorCode)));
	}
}

void Game::PreDraw() {
	// Clear the buffers
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void Game::PostDraw() {
	SwapBuffers(gameWindow->GetDeviceContext());
}

void Game::Initialize() {
}

void Game::LoadContent() {
}

void Game::Update() {
}

void Game::Draw() {
}

void Game::UnloadContent() {
}

void Game::Run() {
	gameWindow = new GameWindow(GetWindowWidth(), GetWindowHeight(), GetWindowX(), GetWindowY(), GetWindowTitle());
	this->PreInitialize();
	this->Initialize();
	this->LoadContent();
	gameWindow->Show();
	MSG msg;
	ZeroMemory(&msg, sizeof(MSG));

	startTime = GetTickCount64();
	DWORD nextDeadLine = startTime + frameTime;
	while (msg.message != WM_QUIT) {
		while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
			// If a message was waiting in the message queue, process it
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		DWORD currentTime = GetTickCount64();
		if (currentTime >= nextDeadLine) {
			totalElapsedTime = currentTime - startTime;
			elapsedTime = (currentTime - nextDeadLine) + frameTime;
			elapsedTimeSeconds = this->elapsedTime / 1000.0;
			this->Update();
			this->PreDraw();
			this->Draw();
			this->PostDraw();
			//nextDeadLine = nextDeadLine + frameTime;
			nextDeadLine = currentTime + frameTime;
		}
	}
	this->UnloadContent();
}

void Game::Exit() {
	PostQuitMessage(0);
}

void Game::SetWindowSize(int width, int height) {
	windowSize.right = width;
	windowSize.bottom = height;
}

void Game::SetWindowPosition(int x, int y) {
	windowSize.top = x;
	windowSize.left = y;
}

void Game::SetWindowTitle(string title) {
	this->windowTitle = title;
}

int Game::GetWindowWidth() const {
	return this->windowSize.right;
}

int Game::GetWindowHeight() const {
	return this->windowSize.bottom;
}

int Game::GetWindowX() const {
	return this->windowSize.left;
}

int Game::GetWindowY() const {
	return this->windowSize.top;
}

string Game::GetWindowTitle() const {
	return this->windowTitle;
}