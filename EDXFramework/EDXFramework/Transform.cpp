#include "Transform.h"

using EDXFramework::Transform;

Transform::Transform() {
	right = glm::vec3(1.0f, 0.0f, 0.0f);
	up = glm::vec3(0.0f, 1.0f, 0.0f);
	forward = glm::vec3(0.0f, 0.0f, 1.0f);
}

Transform::~Transform() {
}

void Transform::Translate(float tx, float ty, float tz) {
	modelMatrix = glm::translate(modelMatrix, glm::vec3(tx, ty, tz));
}

void Transform::Scale(float x, float y, float z) {
	modelMatrix = glm::scale(modelMatrix, glm::vec3(x, y, z));
}

void Transform::Rotate(float x, float y, float z) {
	//TODO: World Transformations

	//Model/Local Transformations
	if (x != 0) {
		glm::quat rightQuat = glm::angleAxis(x, right);
		glm::mat4 rightMatrix = glm::toMat4(rightQuat);
		modelMatrix = modelMatrix * rightMatrix;
		//Update up and forward vectors
		//up = rightQuat * up;
		//forward = rightQuat * forward;
	}
	//Rotate Up
	if (y != 0) {
		glm::quat upQuat = glm::angleAxis(y, up);
		glm::mat4 upMatrix = glm::toMat4(upQuat);
		modelMatrix = modelMatrix * upMatrix;
		//Update right and forward vectors
		//right = upQuat * right;
		//forward = upQuat * forward;
	}
	//Rotate Forward
	if (z != 0) {
		glm::quat forwardQuat = glm::angleAxis(z, forward);
		glm::mat4 forwardMatrix = glm::toMat4(forwardQuat);
		modelMatrix = modelMatrix * forwardMatrix;
		//Update right and forward vectors
		//right = forwardQuat * right;
		//up = forwardQuat * up;
	}
}
