#include "Keyboard.h"

using EDXFramework::Keyboard;

bool Keyboard::IsKeyDown(int virtualKey) {
	return GetKeyState(virtualKey) & 8000;
}