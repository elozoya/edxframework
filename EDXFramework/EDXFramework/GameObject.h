#pragma once
#include "Model.h"
#include "Transform.h"
#include <string>
using namespace std;

namespace EDXFramework {

class GameObject {
public:
	GameObject(string modelPath = "", string texturePath = "");
	virtual ~GameObject();

	void Draw() const;
	void SetModel(Model* model);
	Model* GetModel() const;
	Transform transform;
private:
	Model* model;
};

}
