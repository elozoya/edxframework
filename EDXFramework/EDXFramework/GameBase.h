#pragma once
#include "Game.h"
using namespace EDXFramework;

class GameBase : public Game {
public:
	GameBase();
	virtual ~GameBase();
	void Initialize();
	void LoadContent();
	void Update();
	void Draw();
	void UnloadContent();
};
