#pragma once
#include "EDXFramework.h"

namespace EDXFramework {

class Transform {
public:
	Transform();
	~Transform();
	void Translate(float tx, float ty, float tz);
	void Scale(float x, float y, float z);
	void Rotate(float x, float y, float z);
	glm::mat4 modelMatrix;
private:
	glm::vec3 up;
	glm::vec3 forward;
	glm::vec3 right;
};

}
