#include "EDXFramework.h"
#include "GameObject.h"

using EDXFramework::GameObject;
using EDXFramework::Model;

GameObject::GameObject(string modelPath, string texturePath) {
	model = NULL;
	model = new Model(modelPath, texturePath);
}

GameObject::~GameObject() {
	if (model != NULL) {
		delete model;
	}
}

void GameObject::Draw() const {
	glPushMatrix();
	glMultMatrixf(&transform.modelMatrix[0][0]);
	model->Draw();
	glPopMatrix();
}

void GameObject::SetModel(Model* model) {
	this->model = model;
}

Model* GameObject::GetModel() const {
	return this->model;
}