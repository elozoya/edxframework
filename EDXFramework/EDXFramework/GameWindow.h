#pragma once
#include <Windows.h>
#include <string>
using std::string;

namespace EDXFramework {
namespace GUI {

class GameWindow {
public:
	GameWindow(int width = 800, int height = 600, int x = 0, int y = 0, string title = "Game Window Application");
	~GameWindow();
	void Show();
	HDC GetDeviceContext(void) const;


private:
	static LRESULT CALLBACK WinProc(HWND hWnd,UINT message,WPARAM wParam,LPARAM lParam);
	void ProcessEvent(UINT Message, WPARAM wParam, LPARAM lParam);
	void OnSize(int width, int height);
	HWND hWnd;
	// The window device context
    HDC     hDeviceContext;
	// The openGL context.
    HGLRC   hGLContext;
	void CreateContext();
	string title;
};

}
}