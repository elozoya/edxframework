#pragma once
#include "EDXFramework.h"

namespace EDXFramework {

class Keyboard {
public:
	static bool IsKeyDown(int virtualKey);
};

}