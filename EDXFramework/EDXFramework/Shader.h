#pragma once
#include <Windows.h>
#include <GL/glew.h>
#include <GL\GL.h>
#include <string>
#include <map>
using namespace std;

namespace EDXFramework {
namespace Shaders {

//Esta clase esta en Desarrollo
//Por el momento solo puede:
//*Cargar de Vertex y Fragment Shaders
//*Establecer Uniforms
//Si deseas establecer Attributes utiliza GetShaderProgram y usa tu propio codigo
//para realizarlo
class Shader {
public:
	Shader();
	Shader(string vertexShaderPath, string fragmentShaderPath);
	virtual ~Shader();
	void SetUniform(string uniform, float value);
	void SetUniform(string uniform, float v0, float v1, float v2);
	void SetUniform(string uniform, float v0, float v1, float v2, float v3);
	void Use();
	void Disable();
	GLuint GetShaderProgram();

	static GLuint MakeVertexShader(const char* filename);
	static GLuint MakeFragmentShader(const char* filename);
	static GLuint MakeProgram(GLuint vertexShader, GLuint fragmentShader);
private:
	static char *file_contents(const char *filename, GLint *length);
	static GLuint make_shader(GLenum type, const char *filename);
	static GLuint make_program(GLuint vertex_shader, GLuint fragment_shader);
	static void print_log(GLuint object);
private:
	void InitUniform(string uniform);
	void PushProgramShader();
	void PopProgramShader();
	GLint currentProgramShader;
private:
	GLuint program, vertexShader, fragmentShader;
	map<string, GLint> uniforms;
};

}
}