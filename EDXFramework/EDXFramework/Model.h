#pragma once
#include <string>
using namespace std;
#include "glm.h"
#include "BMPLoader.h"
#include <vector>
#include "Vector3D.h"

namespace EDXFramework {

class Model {
public:
	Model(string modelPath, string texturePath = "");
	virtual ~Model();
	void Draw();
	vector<Vector3D> GetVertices();
	Vector3D GetSize();
	float GetRadio();
private:
	GLMmodel* model;
	BMPClass texture;
	unsigned int textureId;
	unsigned int drawMode;
	vector<Vector3D> vertices;
};

}
