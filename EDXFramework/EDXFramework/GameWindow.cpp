#include "GameWindow.h"
#include <exception>
#include <gl\GL.h>
#include <gl\GLU.h>
#include <math.h>

using namespace EDXFramework::GUI;

#define EDX_WINDOW_CLASS "EDXFramework Window Class"

GameWindow::GameWindow(int width, int height, int x, int y, string title) : title (title) {
	WNDCLASSEX wc;
	ZeroMemory(&wc, sizeof(WNDCLASSEX));
	wc.cbSize        = sizeof(WNDCLASSEX);
	wc.style         = CS_HREDRAW | CS_VREDRAW;
	wc.lpfnWndProc   = &GameWindow::WinProc;
	wc.cbClsExtra    = 0;
	wc.cbWndExtra    = 0;
	wc.hInstance     = GetModuleHandle(NULL);
	wc.hIcon         = NULL;
	wc.hCursor       = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = (HBRUSH)COLOR_WINDOW;
	wc.lpszMenuName  = NULL;
	wc.lpszClassName = EDX_WINDOW_CLASS;
	wc.hIconSm       = NULL;
	if(!RegisterClassEx(&wc)) {
		int nResult = GetLastError();
		throw std::exception("Window class creation failed");
	}

	RECT WindowRect;
	WindowRect.left = x;
	WindowRect.top = y;
	WindowRect.right = width;
	WindowRect.bottom = height;

	DWORD dwExStyle = WS_EX_APPWINDOW | WS_EX_WINDOWEDGE;
	DWORD dwStyle = WS_OVERLAPPEDWINDOW;
	hWnd = CreateWindowEx(dwExStyle,
			EDX_WINDOW_CLASS,
			this->title.c_str(),
			dwStyle,
			WindowRect.left,
			WindowRect.top,
			WindowRect.right - WindowRect.left,
			WindowRect.bottom - WindowRect.top,
			NULL,
			NULL,
			GetModuleHandle(NULL),
			this);
	if (!hWnd)
		throw std::exception("Cannot create the window");

	this->CreateContext();
	// Call OnSize manually because in fullscreen mode it will be
	// called only when the window is created (which is too early
	// because OpenGL is not initialized yet).
	OnSize(width, height);
}

GameWindow::~GameWindow()
{

	if (hGLContext) {
		// Make the rendering context not current
		wglMakeCurrent(NULL, NULL);
		// Delete the OpenGL rendering context
		wglDeleteContext(hGLContext);
		hGLContext = NULL;
	}
	if (hDeviceContext) {
		// Release the device context
		ReleaseDC(hWnd, hDeviceContext);
		hDeviceContext = NULL;
	}

	// Finally, destroy our main window and unregister the
	// window class.
    DestroyWindow(hWnd);
    UnregisterClass(EDX_WINDOW_CLASS, GetModuleHandle(NULL));
}

void GameWindow::Show() {
	int nCmdShow = SW_SHOW;
	ShowWindow(hWnd, nCmdShow);
}

LRESULT GameWindow::WinProc(HWND Handle, UINT Message, WPARAM wParam, LPARAM lParam) {
	if (Message == WM_NCCREATE) {
        // Get the creation parameters.
		CREATESTRUCT* pCreateStruct = reinterpret_cast<CREATESTRUCT*>(lParam);
        // Set as the "user data" parameter of the window
        SetWindowLongPtr(Handle, GWLP_USERDATA, reinterpret_cast<long>(pCreateStruct->lpCreateParams));
	}

    // Get the CMainWindow instance corresponding to the window handle
    GameWindow* gw = reinterpret_cast<GameWindow*>(GetWindowLongPtr(Handle, GWLP_USERDATA));
	if (gw)
		gw->ProcessEvent(Message,wParam,lParam);

    return DefWindowProc(Handle, Message, wParam, lParam);
}

void GameWindow::ProcessEvent(UINT Message, WPARAM wParam, LPARAM lParam) {
    switch (Message) {
        // Quit when we close the main window
        case WM_CLOSE :
            PostQuitMessage(0);
			break;
		case WM_SIZE:
			OnSize(LOWORD(lParam),HIWORD(lParam));
			break;
        case WM_KEYDOWN :
            break;
        case WM_KEYUP :
            break;
    }
}

void GameWindow::OnSize(int width, int height) {
	if (height == 0) {
		return;
	}
	// Sets the size of the OpenGL viewport
    glViewport(0,0,width,height);

	// Select the projection stack and apply
	// an orthographic projection
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	//2D
	//glOrtho(0.0,width,height,0.0,-1.0,1.0);
	//3D
	GLdouble fovY = 45;
	GLdouble zNear = 0.01;
	GLdouble zFar = 1000;
	GLdouble aspect = width / height;
	gluPerspective(fovY, aspect, zNear, zFar);
	glMatrixMode(GL_MODELVIEW);
}

void GameWindow::CreateContext()
{
	// Describes the pixel format of the drawing surface
	PIXELFORMATDESCRIPTOR pfd;
	memset(&pfd, 0, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;	// Version Number
	pfd.dwFlags = PFD_DRAW_TO_WINDOW |	// Draws to a window
				  PFD_SUPPORT_OPENGL |	// The format must support OpenGL
				  PFD_DOUBLEBUFFER;		// Support for double buffering
	pfd.iPixelType = PFD_TYPE_RGBA;		// Uses an RGBA pixel format
	pfd.cColorBits = 32;				// 32 bits colors

	if (!(hDeviceContext = GetDC(hWnd)))
		throw std::exception("Unable to create rendering context");

	int PixelFormat;
	// Do Windows find a matching pixel format ?
	if (!(PixelFormat=ChoosePixelFormat(hDeviceContext, &pfd)))
		throw std::exception("Unable to create rendering context");
	// Set the new pixel format
	if(!SetPixelFormat(hDeviceContext,PixelFormat, &pfd))
		throw std::exception("Unable to create rendering context");
	// Create the OpenGL rendering context
	if (!(hGLContext = wglCreateContext(hDeviceContext)))
		throw std::exception("Unable to create rendering context");
	// Activate the rendering context
	if(!wglMakeCurrent(hDeviceContext, hGLContext))
		throw std::exception("Unable to create rendering context");
}

HDC GameWindow::GetDeviceContext(void) const {
	return this->hDeviceContext;
}