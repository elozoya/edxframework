#include "Vector3D.h"
#include <sstream>

using EDXFramework::Vector3D;
	
/* Constructors */
Vector3D::Vector3D()
{
	x = 0.0;
	y = 0.0;
	z = 0.0;
}

Vector3D::Vector3D(float x, float y, float z)
{
	Vector3D::x = x;
	Vector3D::y = y;
	Vector3D::z = z;
}

Vector3D::Vector3D(const Vector3D &Vector)
{
	x = Vector.x;
	y = Vector.y;
	z = Vector.z;
}

/* ToString() method - Returns vector string */
const char *Vector3D::ToString()
{
	std::ostream o();
	std::ostringstream strStream;
	strStream << "<" << x << ", " << y << ", " << z << ">";
    return strStream.str().c_str();

	/*char *vectorString;
	const char minor = '<';
	const char major = '>';
	const char comma = ',';

	if(!(strStream << x))
		return vectorString;
	std::strcat(vectorString, &minor);
	std::strcat(vectorString, strStream.str().c_str());
	std::strcat(vectorString, &comma);

	if(!(strStream << y))
		return vectorString;
	std::strcat(vectorString, strStream.str().c_str());
	std::strcat(vectorString, &comma);

	if(!(strStream << y))
		return vectorString;
	std::strcat(vectorString, strStream.str().c_str());
	std::strcat(vectorString, &major);

	return vectorString;*/
}

#pragma region operators + - * =
#pragma region equal operators

Vector3D &Vector3D::operator = (const Vector3D &VectorB)
{
    if (this == &VectorB) // Si es el mismo objeto, que se regrese a si mismo
      return *this;
	Vector3D::x = VectorB.x;
	Vector3D::y = VectorB.y;
	Vector3D::z = VectorB.z;

    return *this;
}

/* operator = for same class Vector3D */
//Vector3D &Vector3D::operator = (const Vector3D &VectorB)
//{
//	x = VectorB.x;
//	y = VectorB.y;
//	z = VectorB.z;
//	return *this;
//}

/* operator = for double value */
Vector3D &Vector3D::operator = (const double VectorB)
{
	x = VectorB;
	y = VectorB;
	z = VectorB;
	return *this;
}

/* operator = for double array [3] */
Vector3D &Vector3D::operator = (const double VectorB[])
{
	x = VectorB[0];
	y = VectorB[1];
	z = VectorB[2];
	return *this;
}
#pragma endregion

#pragma region addition operators
/* operator + for same class Vector3D */
Vector3D &Vector3D::operator + (const Vector3D &VectorB)
{
	Vector3D tmp(*this);
	tmp.x = x + VectorB.x;
	tmp.y = y + VectorB.y;
	tmp.z = z + VectorB.z;
	return tmp;
}

/* operator + for double value*/
Vector3D &Vector3D::operator + (const double VectorB)
{
	Vector3D tmp;
	tmp.x = x + VectorB;
	tmp.y = y + VectorB;
	tmp.z = z + VectorB;
	return tmp;
}

/* operator + for double array [3] */
Vector3D &Vector3D::operator + (const double VectorB[])
{
	Vector3D tmp;
	tmp.x = x + VectorB[0];
	tmp.y = y + VectorB[1];
	tmp.z = z + VectorB[2];
	return tmp;
}
#pragma endregion

#pragma region subtraction operators
/* operator - for same class Vector3D */
Vector3D &Vector3D::operator - (const Vector3D &VectorB)
{
	Vector3D tmp(*this);
	tmp.x = x - VectorB.x;
	tmp.y = y - VectorB.y;
	tmp.z = z - VectorB.z;
	return tmp;
}

/* operator - for double value*/
Vector3D &Vector3D::operator - (const double VectorB)
{
	Vector3D tmp;
	tmp.x = x - VectorB;
	tmp.y = y - VectorB;
	tmp.z = z - VectorB;
	return tmp;
}

/* operator - for double array [3] */
Vector3D &Vector3D::operator - (const double VectorB[])
{
	Vector3D tmp;
	tmp.x = x - VectorB[0];
	tmp.y = y - VectorB[1];
	tmp.z = z - VectorB[2];
	return tmp;
}

#pragma endregion

#pragma region product operators
/* operator * for same class Vector3D */
Vector3D &Vector3D::operator * (const Vector3D &VectorB)
{
	Vector3D tmp(*this);
	tmp.x = (this->y * VectorB.z) - (this->z * VectorB.y);
	tmp.y = (this->z * VectorB.x) - (this->x * VectorB.z);
	tmp.z = (this->x * VectorB.y) - (this->y * VectorB.x);
	return tmp;
}

/* operator * for double value*/
Vector3D &Vector3D::operator * (const double VectorB)
{
	Vector3D tmp;
	tmp.x = (y * VectorB) - (z * VectorB);
	tmp.y = (z * VectorB) - (x * VectorB);
	tmp.z = (x * VectorB) - (y * VectorB);
	return tmp;
}

/* operator * for double array [3] */
Vector3D &Vector3D::operator * (const double VectorB[])
{
	if((sizeof(VectorB) / sizeof(double)) == 3)
	{
		Vector3D tmp;
		tmp.x = (y * VectorB[2]) - (z * VectorB[1]);
		tmp.y = (z * VectorB[0]) - (x * VectorB[2]);
		tmp.z = (x * VectorB[1]) - (y * VectorB[0]);
		return tmp;
	}
}
/* operator *     "dot product" */
float Vector3D::operator * (Vector3D v)
{
	return (this->x*v.x+this->y*v.y+this->z*v.z);
}

#pragma endregion
#pragma endregion

void Vector3D::Normalize()
{
	Vector3D tmp;

	float length = GetLength();
	if (length == 0.0f) 
		return;//return Vector3D(0.0f, 0.0f, 0.0f);

	this->x /= length;
	this->y /= length;
	this->z /= length;
}

Vector3D CrossProduct (Vector3D *u, Vector3D *v)
{
	Vector3D resVector;

	resVector.x = u->y*v->z - u->z*v->y;
	resVector.y = u->z*v->x - u->x*v->z;
	resVector.z = u->x*v->y - u->y*v->x;

	return resVector;
}

float Vector3D::GetLength()
{
	return (float)(sqrt(sqrt(this->x)+sqrt(this->y)+sqrt(this->z)));
}