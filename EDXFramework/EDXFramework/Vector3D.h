#pragma once
#ifndef _VECTOR3D_H_
#define _VECTOR3D_H_

namespace EDXFramework {

class Vector3D {
public:
	float x;
	float y;
	float z;

	Vector3D();
	Vector3D(float x, float y, float z);
	Vector3D(const Vector3D &Vector);

	Vector3D &operator = (const Vector3D &VectorB);
	Vector3D &operator = (const double VectorB);
	Vector3D &operator = (const double VectorB[]);

	Vector3D &operator + (const Vector3D &VectorB);
	Vector3D &operator + (const double VectorB);
	Vector3D &operator + (const double VectorB[]);

	Vector3D &operator - (const Vector3D &VectorB);
	Vector3D &operator - (const double VectorB);
	Vector3D &operator - (const double VectorB[]);

	float operator * (Vector3D v); //producto punto
	Vector3D &operator * (const Vector3D &VectorB);
	Vector3D &operator * (const double VectorB);
	Vector3D &operator * (const double VectorB[]);
		

	void Normalize();
	float GetLength();

	const char* ToString();
};

}
#endif