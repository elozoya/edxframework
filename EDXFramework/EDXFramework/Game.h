#pragma once
#include "EDXFramework.h"
#include "GameWindow.h"
#include <string>

using EDXFramework::GUI::GameWindow;
using std::string;

namespace EDXFramework {

class Game {
public:
	Game();
	virtual ~Game();
	virtual void Initialize();
	virtual void LoadContent();
	virtual void Update();
	virtual void Draw();
	virtual void UnloadContent();
	void Run();
	void Exit();
	void SetWindowSize(int width, int height);
	void SetWindowPosition(int x, int y);
	void SetWindowTitle(string title);
	int GetWindowWidth() const;
	int GetWindowHeight() const;
	int GetWindowX() const;
	int GetWindowY() const;
	string GetWindowTitle() const;

protected:
	DWORD startTime;
	DWORD totalElapsedTime;
	DWORD elapsedTime;
	float elapsedTimeSeconds;
private:
	int frameTime;
	GameWindow* gameWindow;
	RECT windowSize;
	void PreInitialize();
	void PostDraw();
	void PreDraw();
	string windowTitle;
};

}