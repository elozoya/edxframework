#include "Shader.h"
#include <iostream>

using namespace EDXFramework::Shaders;

Shader::Shader() {
}

Shader::Shader(string vertexShaderPath, string fragmentShaderPath) {
	vertexShader = Shader::MakeVertexShader(vertexShaderPath.c_str());
	fragmentShader = Shader::MakeFragmentShader(fragmentShaderPath.c_str());
	program = Shader::MakeProgram(vertexShader, fragmentShader);
}

Shader::~Shader() {
	//if (glIsProgram(this->program)) {
		//glDeleteProgram(this->program);
	//}
}

void Shader::InitUniform(string uniform) {
	uniforms[uniform] = glGetUniformLocation(program, uniform.c_str());
	if (uniforms[uniform] == -1) {
		throw string("Could not bind uniform ") + uniform;
	}
}

void Shader::SetUniform(string uniform, float value) {
	if (uniforms.find(uniform) == uniforms.end()) {
		InitUniform(uniform);
	}
	this->PushProgramShader();
	this->Use();
	glUniform1f(uniforms[uniform], value);
	this->Disable();
	this->PopProgramShader();
}

void Shader::SetUniform(string uniform, float v0, float v1, float v2) {
	if (uniforms.find(uniform) == uniforms.end()) {
		InitUniform(uniform);
	}
	this->PushProgramShader();
	this->Use();
	glUniform3f(uniforms[uniform], v0, v1, v2);
	this->Disable();
	this->PopProgramShader();
}

void Shader::SetUniform(string uniform, float v0, float v1, float v2, float v3) {
	if (uniforms.find(uniform) == uniforms.end()) {
		InitUniform(uniform);
	}
	this->PushProgramShader();
	this->Use();
	glUniform4f(uniforms[uniform], v0, v1, v2, v3);
	this->Disable();
	this->PopProgramShader();
}

void Shader::Use() {
	glUseProgram(program);
}

void Shader::Disable() {
	glUseProgramObjectARB(0);
}

GLuint Shader::GetShaderProgram() {
	return program;
}

char * Shader::file_contents(const char *filename, GLint *length) {
    FILE *f = fopen(filename, "r");
    char *buffer;
    if (!f) {
        fprintf(stderr, "Unable to open %s for reading\n", filename);
        return NULL;
	}
    fseek(f, 0, SEEK_END);
    *length = ftell(f);
    fseek(f, 0, SEEK_SET);

    buffer = (char*)malloc(*length+1);
    *length = fread(buffer, 1, *length, f);
    fclose(f);
    ((char*)buffer)[*length] = '\0';
    return buffer;
}

GLuint Shader::make_shader(GLenum type, const char *filename) {
    GLint length;
    GLchar *source = file_contents(filename, &length);
	//char *source = file_contents(filename, &length);
    GLuint shader;
    GLint shader_ok;
    if (!source)
        return 0;
    shader = glCreateShader(type);
    glShaderSource(shader, 1, (const GLchar**)&source, &length);
	//glShaderSource(shader, 1, &source, &length);
    free(source);
    glCompileShader(shader);
    glGetShaderiv(shader, GL_COMPILE_STATUS, &shader_ok);
    if (!shader_ok) {
        //fprintf(stderr, "Failed to compile %s:\n", filename);
        //show_info_log(shader, glGetShaderiv, glGetShaderInfoLog);
		print_log(shader);
        glDeleteShader(shader);
        return 0;
    }
    return shader;
}

GLuint Shader::make_program(GLuint vertex_shader, GLuint fragment_shader) {
    GLint program_ok;
    GLuint program = glCreateProgram();
    glAttachShader(program, vertex_shader);
    glAttachShader(program, fragment_shader);
    glLinkProgram(program);
    glGetProgramiv(program, GL_LINK_STATUS, &program_ok);
    if (!program_ok) {
        //fprintf(stderr, "Failed to link shader program:\n");
        //show_info_log(program, glGetProgramiv, glGetProgramInfoLog);
		print_log(program);
        glDeleteProgram(program);
        return 0;
    }
    return program;
}

void Shader::print_log(GLuint object) {
  GLint log_length = 0;
  if (glIsShader(object))
    glGetShaderiv(object, GL_INFO_LOG_LENGTH, &log_length);
  else if (glIsProgram(object))
    glGetProgramiv(object, GL_INFO_LOG_LENGTH, &log_length);
  else {
    fprintf(stderr, "printlog: Not a shader or a program\n");
    return;
  }

  char* log = (char*)malloc(log_length);

  if (glIsShader(object))
    glGetShaderInfoLog(object, log_length, NULL, log);
  else if (glIsProgram(object))
    glGetProgramInfoLog(object, log_length, NULL, log);

  fprintf(stderr, "%s", log);
  free(log);
}

GLuint Shader::MakeVertexShader(const char* filename) {
	return make_shader(GL_VERTEX_SHADER, filename);
}

GLuint Shader::MakeFragmentShader(const char* filename) {
	return make_shader(GL_FRAGMENT_SHADER, filename);
}

GLuint Shader::MakeProgram(GLuint vertexShader, GLuint fragmentShader) {
	return make_program(vertexShader, fragmentShader);
}

void Shader::PushProgramShader() {
	glGetIntegerv(GL_CURRENT_PROGRAM, &this->currentProgramShader);
}

void Shader::PopProgramShader() {
	glUseProgram(this->currentProgramShader);
}