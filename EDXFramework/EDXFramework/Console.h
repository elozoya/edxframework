#pragma once
#include <Windows.h>
#include <io.h>
#include <fcntl.h>
#include <iostream>
using namespace std;

namespace EDXFramework {

class Console {
public:
	template <class T>
	static void Log(T msg) {
		Console::init();
		cout << msg << endl;
	}
private:
	static void init() {
		static bool initialized = false;
		if (!initialized) {
			initialized = true;
			initConsole();
		}
	}
	static void initConsole() {
		AllocConsole();
		HANDLE handle_out = GetStdHandle(STD_OUTPUT_HANDLE);
		int hCrt = _open_osfhandle((long) handle_out, _O_TEXT);
		FILE* hf_out = _fdopen(hCrt, "w");
		setvbuf(hf_out, NULL, _IONBF, 1);
		*stdout = *hf_out;

		HANDLE handle_in = GetStdHandle(STD_INPUT_HANDLE);
		hCrt = _open_osfhandle((long) handle_in, _O_TEXT);
		FILE* hf_in = _fdopen(hCrt, "r");
		setvbuf(hf_in, NULL, _IONBF, 128);
		*stdin = *hf_in;
	}
};

}