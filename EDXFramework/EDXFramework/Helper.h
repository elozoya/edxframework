#pragma once
#include <iostream>
#include <sstream>
using namespace std;

namespace EDXFramework {

class Helper {
public:
	template <class T>
	static string toString(T object) {
		ostringstream os;
		os << object;
		return os.str();
	}
private:
};

}