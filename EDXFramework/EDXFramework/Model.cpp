#include "Model.h"
#include <exception>
using namespace std;
using namespace EDXFramework;

Model::~Model() {
	glmDelete(model);
}

Model::Model(string modelPath, string texturePath) : textureId(0) {
	drawMode = GLM_SMOOTH | GLM_MATERIAL;
	//Load 3D Model
	try {
		model = glmReadOBJ((char*)modelPath.c_str());
	} catch (exception& e) {
		MessageBox(NULL, e.what(), NULL, NULL);
		exit(1);
	}
	//glmUnitize(model);
	glmFacetNormals(model);
	glmVertexNormals(model, 90.0);
	//Load Texture
	BMPError e = BMPLoad(texturePath, texture);;
	if (e == BMPNOERROR) {
		drawMode |= GLM_TEXTURE;
		glGenTextures(1, &textureId);
		glBindTexture(GL_TEXTURE_2D, textureId);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_CLAMP);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_CLAMP);
		glTexImage2D(GL_TEXTURE_2D, 0, 3, texture.width, texture.height, 0, GL_RGB,GL_UNSIGNED_BYTE, texture.bytes);
	}
}

void Model::Draw() {
	if (drawMode & GLM_TEXTURE) {
		glBindTexture(GL_TEXTURE_2D, textureId);
	}
	glmDraw(model, drawMode);
	glBindTexture(GL_TEXTURE_2D, NULL);
}

vector<Vector3D> Model::GetVertices() {
	if (this->vertices.size() > 0) {
		return this->vertices;
	}
	for(int i = 1; i <= model->numvertices; i++) {
		Vector3D v;
		v.x = model->vertices[3 * i + 0];
		v.y = model->vertices[3 * i + 1];
		v.z = model->vertices[3 * i + 2];
		vertices.push_back(v);
	}
	return vertices;
}

Vector3D Model::GetSize() {
	GLfloat dimensions[3];
	glmDimensions(this->model, dimensions);
	return Vector3D(dimensions[0], dimensions[1], dimensions[2]);
}

float Model::GetRadio() {
	GLfloat dimensions[3];
	glmDimensions(this->model, dimensions);
	float radio = dimensions[0];
	radio = (dimensions[1] > radio) ? dimensions[1] : radio;
	radio = (dimensions[2] > radio) ? dimensions[2] : radio;
	return radio;
}

