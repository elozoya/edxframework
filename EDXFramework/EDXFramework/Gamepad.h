#pragma once
#ifndef _GAMEPAD_H_
#define _GAMEPAD_H_

#define WIN32_LEAN_AND_MEAN

#include <windows.h>

enum GAMEPAD_TYPE
{
	XBOX_CONTROLLER		= 1,
	GENERIC_CONTROLLER	= 2,
	NONE				= 3
};

struct GAMEPAD_STATE
{
	bool DPAD_UP;
	bool DPAD_RIGHT;
	bool DPAD_DOWN;
	bool DPAD_LEFT;

	bool A;
	bool B;
	bool Y;
	bool X;

	bool START;
	bool BACK;

	bool LEFT_THUMB;
	bool RIGHT_THUMB;

	float THUMB_LX;
	float THUMB_LY;

	float THUMB_RX;
	float THUMB_RY;

	bool LEFT_SHOULDER;
	bool RIGHT_SHOULDER;

	float LEFT_TRIGGER;
	float RIGHT_TRIGGER;
};

class Gamepad
{
protected:
	GAMEPAD_TYPE	gamepad_type;
	GAMEPAD_STATE	gamepad_state;
public:
	Gamepad()
	{
		gamepad_type					= GAMEPAD_TYPE::NONE;
		gamepad_state.A					= false;
		gamepad_state.B					= false;
		gamepad_state.Y					= false;
		gamepad_state.X					= false;
		gamepad_state.BACK				= false;
		gamepad_state.START				= false;
		gamepad_state.DPAD_UP			= false;
		gamepad_state.DPAD_RIGHT		= false;
		gamepad_state.DPAD_DOWN			= false;
		gamepad_state.LEFT_SHOULDER		= false;
		gamepad_state.RIGHT_SHOULDER	= false;
		gamepad_state.LEFT_THUMB		= false;
		gamepad_state.RIGHT_THUMB		= false;
		gamepad_state.LEFT_TRIGGER		= 0.0f;
		gamepad_state.RIGHT_TRIGGER		= 0.0f;
		gamepad_state.THUMB_LX			= 0.0f;
		gamepad_state.THUMB_LY			= 0.0f;
		gamepad_state.THUMB_RX			= 0.0f;
		gamepad_state.THUMB_RY			= 0.0f;
	}
	virtual ~Gamepad(){};

	virtual GAMEPAD_STATE GetState() = 0;
	virtual bool IsConnected() = 0;
	virtual void Vibrate(int leftValue, int rightValue) = 0;
	virtual void Update() = 0;
};

#endif