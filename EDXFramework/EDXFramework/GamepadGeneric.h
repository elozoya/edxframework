#pragma once
#ifndef _GENERIC_CONTROLLER_H_
#define _GENERIC_CONTROLLER_H_
#include <windows.h>
#include <commctrl.h>
#include <dinput.h>
#include <dinputd.h>

#include "Gamepad.h"

class GamepadGeneric : public Gamepad
{
public:
	GamepadGeneric();
	~GamepadGeneric();

	GAMEPAD_STATE GetState();
	void Update();
	void Vibrate(int leftValue, int rightValue);
	bool IsConnected();
};
#endif
